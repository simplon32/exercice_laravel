@extends('base')

@section('title', 'accueil')

@section('content')
    <h1>Mon Blog</h1>
    {{'ko<strong>je suis pas gras</strong>'}}
    <p> voici mon blog</p>

    @foreach($posts as $post)
        <article>
            <h2>{{$post->title}}</h2>
            <p>{{$post->content}}</p>
            <p><a href="{{ route('blog.show', ['slug'=>$post->slug, 'id'=>$post->id])}}">Lire la suite</a></p>
        </article>
    @endforeach

@endsection
