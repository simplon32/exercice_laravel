<?php

use App\Http\Controllers\postController;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function(){
    return view('welcome');
});

Route::prefix('/blog')->name('blog.')->controller(postController::class)->group(function () {
    Route::get('/', 'index');
        // $post= new Post();
        // $post->title = 'Titre de mon Neme article';
        // $post->slug = 'Mon Neme article';
        // $post->content = 'Mon Neme contenu';
        // $post->save();
                    // $post= Post::find(1);
                    // $post->title = 'nouveau titre';
                    // $post->save();
        //  $posts =  Post::paginate(1,['id','title', 'content']);

        //  $posts =  Post::all(['id','title']);
        // $posts = Post::findOrFail(3);
        // dd($posts);
        // return $posts;
                    //     return[
                    //         "Link" => \route('blog.show',['slug' =>'article','id' =>13]),
                    //     ];
                    // })->name('index');

    // Route::get('/nouveau_post', function (Request $request) {
    //     $nveauPost = Post::create([
    //         'title' => 'je suis un nouveau titre',
    //         'content' => 'je suis un nouveau contenu',
    //         'slug' => 'nouveau-post'
    //     ]);
    //     return $nveauPost;
    // });

    Route::get('/{slug}-{id}','show')
    ->where([
            'id' => '[0-9]+',
            'slug' => '[a-z0-9-]+'
        ])->name('show');
                // $post= Post::find($id);
                // return $post;
        // return[
        //     "slug" =>$slug,
        //     'id' => $id,
        //     'name' =>$request->input('name'),
        //     'title' => 'Mon Neme article',
        //     'content' => 'Ceci est le Neme contenu'
        // ];
            // }

});

